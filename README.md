# Local HTTPS Express Server

## Setup

1. Run `npm install`
2. Generate self-signed SSL certificates (`openssl req -nodes -new -x509 -keyout server.key -out server.cert`) or use the one in the repository
3. Modify your `hosts` file (probably located in `/etc` folder) and add an entry to redirect the IP 127.0.0.1 to an alias of your linking (DO NOT DELETE THE `localhost` ENTRY)
4. Start the server with `npm start`
5. Open your browser in `https://<you_alias>:3000`
6. Configure whichever browser you are using to allow insecure certificates on that site

That's it. Have fun.
